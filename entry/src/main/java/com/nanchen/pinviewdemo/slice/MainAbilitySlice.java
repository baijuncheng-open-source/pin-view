package com.nanchen.pinviewdemo.slice;

import com.nanchen.pinview.PinView;
import com.nanchen.pinviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.IntervalTimer;

public class MainAbilitySlice extends AbilitySlice implements PinView.PinViewEventListener {

    private Text mTvTime;
    private PinView mPinView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mTvTime = (Text) findComponentById(ResourceTable.Id_tv_time_cut);
        mPinView = (PinView)findComponentById(ResourceTable.Id_pinView);

//        mPinView.setCanInput(true);// 设置是否可输入 默认true
//        mPinView.setHint("");      // 设置EditText的Hint
//        mPinView.setInputType(PinView.InputType.NUMBER); // 设置输入的格式
//        mPinView.setPassword(true); // 设置是否以明文显示
//        mPinView.setPinHeight(60);  // 设置每一个EditText的高度
//        mPinView.setPinWidth(60);   // 设置每一个EditText的宽度
//        mPinView.setPinLength(4);   // 设置可输入的 PIN 码长度，默认4
//        mPinView.clearValue();      // 清除输入的数据
//        mPinView.setValue("1234");  // 设置输入值
        mPinView.setPinViewEventListener(this); // 设置输入完毕的监听事件
    }

    int count = 0;

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    MyTimeCount mTimeCount;

    private class MyTimeCount extends IntervalTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #schedule()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onInterval(long)} callbacks.
         */
        public MyTimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onInterval(long millisUntilFinished) {
            new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                @Override
                public void run() {
                    mTvTime.setText("输入错误，" + (millisUntilFinished / 1000) + "秒后再试");
                }
            });
        }

        @Override
        public void onFinish() {
            new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                @Override
                public void run() {
                    mTvTime.setText("输入密码");
                    mPinView.setCanInput(true);
                }
            });
        }
    }

    @Override
    public void onDataEntered(PinView pinview, boolean fromUser) {
        new ToastDialog(this).setText(pinview.getValue()).setDuration(1500).show();
//        String value = pinview.getValue();
//
//        String trueValue = getSharedPreferences(GlobalConfig.PREFERENCES_KEY, MODE_PRIVATE).getString(GlobalConfig.PIN_KEY, null);
//        if (TextUtils.equals(trueValue, value)) {
//            // 如果输入正确，进入到主页
//            startActivity(new Intent(this, HomeActivity.class));
//            finish();
//        } else {
        count++;
        mPinView.setValue("");
        // 清空
        mTvTime.setText("输入错误，还可尝试" + (5 - count) + "次");
        if (count >= 5) { // 大于5次需要等待10秒
            long millisInFuture = 10 * 1000;
            if (mTimeCount != null)
                mTimeCount.cancel();
            mTimeCount = new MyTimeCount(millisInFuture, 1000);
            mTimeCount.schedule();
            mPinView.setCanInput(false);
        }
//        }
    }


}
