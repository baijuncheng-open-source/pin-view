package com.nanchen.pinview;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.components.InputAttribute;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class PinView extends DirectionalLayout implements Text.TextObserver, Component.KeyEventListener, Component.FocusChangedListener {
    private final float DENSITY = getContext().getResourceManager().getDeviceCapability().screenDensity;
    private int mPinLength = 4; // 默认的Pin长度
    private List<TextField> editTextList = new ArrayList<>();
    private int mPinWidth = 50;
    private int mPinHeight = 50;
    private int mSplitWidth = 20;
    private boolean mCursorVisible = false;
    private boolean mDelPressed = false;

    private int mPinBackground = ResourceTable.Graphic_sample_background;
    private boolean mPassword = false;
    private String mHint = "";
    private InputType inputType = InputType.NUMBER;
    private boolean finalNumberPin = false;
    private PinViewEventListener mListener;
    private boolean fromSetValue = false;

    public enum InputType {
        TEXT, NUMBER
    }

    /**
     * Interface for onDataEntered event.
     */

    public interface PinViewEventListener {
        void onDataEntered(PinView pinview, boolean fromUser);
    }

    ClickedListener mClickListener;

    Component currentFocus = null;

    DirectionalLayout.LayoutConfig params;


    public PinView(Context context) {
        this(context, null);
    }

    public PinView(Context context, AttrSet attrs) {
        super(context, attrs);
        setAlignment(LayoutAlignment.CENTER);
        init(context, attrs);
    }


    /**
     * A method to take care of all the initialisations.
     *
     * @param context
     * @param attrs
     */
    private void init(Context context, AttrSet attrs) {
        this.removeAllComponents();
        mPinHeight *= DENSITY;
        mPinWidth *= DENSITY;
        mSplitWidth *= DENSITY;
        initAttributes(context, attrs);
        params = new LayoutConfig(mPinWidth, mPinHeight);
        setOrientation(HORIZONTAL);
        createEditTexts();
        super.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean focused = false;
                for (TextField editText : editTextList) {
                    if (editText.length() == 0) {
                        editText.requestFocus();
                        focused = true;
                        break;
                    }
                }
                if (!focused && editTextList.size() > 0) { // Focus the last view
                    editTextList.get(editTextList.size() - 1).requestFocus();
                }
                if (mClickListener != null) {
                    mClickListener.onClick(PinView.this);
                }
            }
            });

        final Component firstEditText = editTextList.get(0);
        if (firstEditText != null) {
            firstEditText.requestFocus();
        }
        updateEnabledState();
    }

    /**
     * Creates editTexts and adds it to the pinview based on the pinLength specified.
     */

    private void createEditTexts() {
        removeAllComponents();
        editTextList.clear();
        TextField editText;
        for (int i = 0; i < mPinLength; i++) {
            editText = new TextField(getContext());
            editTextList.add(i, editText);
            this.addComponent(editText);
            generateOneEditText(editText, "" + i);
        }
        setTransformation();
    }

    @Override
    public void setEnabled(boolean enabled) {
        for (TextField editText : editTextList) {
            editText.setEnabled(enabled);
        }
    }

    @Override
    public void setFocusable(int focusable) {
        for (TextField textField : editTextList) {
            textField.setFocusable(focusable);
        }
        if (focusable == FOCUS_ENABLE) {
            editTextList.get(0).setTouchFocusable(true);
            editTextList.get(0).requestFocus();
        }
    }

    @Override
    public void setTouchFocusable(boolean focusableInTouchMode) {
        super.setTouchFocusable(focusableInTouchMode);
        for (TextField editText : editTextList) {
            editText.setEnabled(focusableInTouchMode);
        }
    }

    /** @noinspection checkstyle:Indentation*/
    public void setCanInput(boolean canInput) {
        setEnabled(canInput);
        if (canInput) {
            setFocusable(FOCUS_ENABLE);
        } else {
            setFocusable(FOCUS_DISABLE);

        }
        if (!canInput) {
            for (TextField editText : editTextList) {
                editText.setKeyEventListener(null);
            }
        } else {
            for (TextField editText : editTextList) {
                editText.setKeyEventListener(this);
            }
        }
    }


    /**
     * This method gets the attribute values from the XML, if not found it takes the default values.
     *
     * @param context
     * @param attrs
     */
    private void initAttributes(Context context, AttrSet attrs) {
        if (attrs != null) {
            if (attrs.getAttr("pinBackground").isPresent()) {
                mPinBackground = attrs.getAttr("pinBackground").get().getIntegerValue();
            }

            if (attrs.getAttr("pinLength").isPresent()) {
                mPinLength = attrs.getAttr("pinLength").get().getIntegerValue();
            }

            if (attrs.getAttr("pinHeight").isPresent()) {
                mPinHeight = attrs.getAttr("pinHeight").get().getDimensionValue();
            }

            if (attrs.getAttr("pinWidth").isPresent()) {
                mPinWidth = attrs.getAttr("pinWidth").get().getDimensionValue();
            }

            if (attrs.getAttr("splitWidth").isPresent()) {
                mSplitWidth = attrs.getAttr("splitWidth").get().getDimensionValue();
            }

            if (attrs.getAttr("cursorVisible").isPresent()) {
                mCursorVisible = attrs.getAttr("cursorVisible").get().getBoolValue();
            }

            if (attrs.getAttr("password").isPresent()) {
                mPassword = attrs.getAttr("password").get().getBoolValue();
            }

            if (attrs.getAttr("hint").isPresent()) {
                mHint = attrs.getAttr("hint").get().getStringValue();
            }

            InputType[] its = InputType.values();
            if (attrs.getAttr("inputType").isPresent()) {
                if ("number".equals(attrs.getAttr("inputType").get().getStringValue())) {
                    inputType = its[1];
                } else  {
                    inputType = its[0];
                }
            } else {
                inputType = its[0];
            }

        }
    }

    /**
     * Takes care of styling the editText passed in the param.
     * tag is the index of the editText.
     *
     * @param styleEditText
     * @param tag
     */
    private void generateOneEditText(TextField styleEditText, String tag) {
        params.setMargins(mSplitWidth / 2, mSplitWidth / 2, mSplitWidth / 2, mSplitWidth / 2);
        styleEditText.setLayoutConfig(params);
        styleEditText.setTextAlignment(TextAlignment.CENTER);
        styleEditText.setTextCursorVisible(mCursorVisible);
        styleEditText.setTextColor(new Color(Color.getIntColor("#FFFFFF")));

        if (!mCursorVisible) {
            styleEditText.setHint(mHint);

            styleEditText.setTouchEventListener(new TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                    // When back space is pressed it goes to delete mode and when u click on an edit Text it should get out of the delete mode
                    mDelPressed = false;
                    return false;
                }
            });
        }
        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED}, new ShapeElement(getContext(), ResourceTable.Graphic_example_drawable_with_grey_focused));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, new ShapeElement(getContext(), ResourceTable.Graphic_example_drawable_with_grey_disabled));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, new ShapeElement(getContext(), ResourceTable.Graphic_example_drawable_with_grey_empty));
        styleEditText.setBackground(stateElement);
        styleEditText.setPadding(0, 0, 0, 0);
        styleEditText.setTag(tag);
        styleEditText.setTextInputType(getKeyboardInputType());
        styleEditText.addTextObserver(this);
        styleEditText.setFocusChangedListener(this);
        styleEditText.setKeyEventListener(this);
    }

    private int getKeyboardInputType() {
        int it;
        switch (inputType) {
            case NUMBER:
                it = InputAttribute.PATTERN_PASSWORD | InputAttribute.PATTERN_NUMBER;
                break;
            case TEXT:
                it = InputAttribute.PATTERN_TEXT;
                break;
            default:
                it = InputAttribute.PATTERN_TEXT;
        }
        return it;
    }

    /**
     * Returns the value of the PinView
     *
     * @return
     */

    public String getValue() {
        StringBuilder sb = new StringBuilder();
        for (TextField et : editTextList) {
            sb.append(et.getText().toString());
        }
        return sb.toString();
    }

    /**
     * Requsets focus on current pin view and opens keyboard if forceKeyboard is enabled.
     *
     * @return the current focused pin view. It can be used to open softkeyboard manually.
     */
    public Component requestPinEntryFocus() {
        int currentTag = Math.max(0, getIndexOfCurrentFocus());
        TextField currentEditText = editTextList.get(currentTag);
        if (currentEditText != null) {
            currentEditText.requestFocus();
        }
        return currentEditText;
    }

    /**
     * Clears the values in the PinView
     */
    public void clearValue() {
        setValue("");
    }

    /**
     * Sets the value of the PinView
     *
     * @param value
     */
    public void setValue(String value) {
        String regex = "[0-9]*"; // Allow empty string to clear the fields
        fromSetValue = true;
        if (inputType == InputType.NUMBER && !value.matches(regex))
            return;
        int lastTagHavingValue = -1;
        for (int i = 0; i < editTextList.size(); i++) {
            if (value.length() > i) {
                lastTagHavingValue = i;
                editTextList.get(i).setText(((Character) value.charAt(i)).toString());
            } else {
                editTextList.get(i).setText("");
            }
        }
        if (mPinLength > 0) {
            if (lastTagHavingValue < mPinLength - 1) {
                currentFocus = editTextList.get(lastTagHavingValue + 1);
            } else {
                currentFocus = editTextList.get(mPinLength - 1);
                if (inputType == InputType.NUMBER || mPassword)
                    finalNumberPin = true;
                if (mListener != null)
                    mListener.onDataEntered(this, false);
            }
            currentFocus.requestFocus();
        }
        fromSetValue = false;
        updateEnabledState();
    }

    @Override
    public void onFocusChange(Component view, boolean isFocused) {
        if (isFocused && !mCursorVisible) {
            if (mDelPressed) {
                currentFocus = view;
                mDelPressed = false;
                return;
            }
            for (final TextField editText : editTextList) {
                if (editText.length() == 0) {
                    if (editText != view) {
                        editText.requestFocus();
                    } else {
                        currentFocus = view;
                    }
                    return;
                }
            }
            if (editTextList.get(editTextList.size() - 1) != view) {
                editTextList.get(editTextList.size() - 1).requestFocus();
            } else {
                currentFocus = view;
            }
        } else if (isFocused && mCursorVisible) {
            currentFocus = view;
        } else {
            view.clearFocus();
        }
    }

    /**
     * Handles the character transformation for password inputs.
     */
    private void setTransformation() {
        if (mPassword) {
            for (TextField editText : editTextList) {
                editText.removeTextObserver(this);
                editText.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                editText.addTextObserver(this);
            }
        } else {
            for (TextField editText : editTextList) {
                editText.removeTextObserver(this);
                editText.setTextInputType(InputAttribute.PATTERN_NUMBER);
                editText.addTextObserver(this);
            }
        }
    }

    /**
     * Fired when text changes in the editTexts.
     * Backspace is also identified here.
     *
     * @param charSequence
     * @param start
     * @param i1
     * @param count
     */
    @Override
    public void onTextUpdated(String charSequence, int start, int i1, int count) {
        if (charSequence.length() == 1 && currentFocus != null) {
            final int currentTag = getIndexOfCurrentFocus();
            if (currentTag < mPinLength - 1) {
                TextField nextEditText = editTextList.get(currentTag + 1);
                nextEditText.setEnabled(true);
                nextEditText.setTouchFocusable(true);
                nextEditText.requestFocus();

            } else {
                //Last Pin box has been reached.
            }
            if (currentTag == mPinLength - 1 && inputType == InputType.NUMBER || currentTag == mPinLength - 1 && mPassword) {
                finalNumberPin = true;
            }

        } else if (charSequence.length() == 0) {
            int currentTag = getIndexOfCurrentFocus();
            mDelPressed = true;
            //For the last cell of the non password text fields. Clear the text without changing the focus.
            if (editTextList.get(currentTag).getText().length() > 0)
                editTextList.get(currentTag).setText("");
        }

        for (int index = 0; index < mPinLength; index++) {
            if (editTextList.get(index).getText().length() < 1)
                break;
            if (!fromSetValue && index + 1 == mPinLength && mListener != null) {
                mListener.onDataEntered(this, true);
            }
        }
        updateEnabledState();
    }

    /**
     * Disable views ahead of current focus, so a selector can change the drawing of those views.
     */
    private void updateEnabledState() {
        int currentTag = Math.max(0, getIndexOfCurrentFocus());
        for (int index = 0; index < editTextList.size(); index++) {
            TextField editText = editTextList.get(index);
            editText.setEnabled(index <= currentTag);
        }
    }

    /**
     * Monitors keyEvent.
     *
     * @param view
     * @param keyEvent
     * @return
     */
    @Override
    public boolean onKeyEvent(Component view, KeyEvent keyEvent) {
        if (keyEvent.isKeyDown() && keyEvent.getKeyCode() == KeyEvent.KEY_DEL) {
            // Perform action on Del press
            int currentTag = getIndexOfCurrentFocus();
            //Last tile of the number pad. Clear the edit text without changing the focus.
            if (inputType == InputType.NUMBER && currentTag == mPinLength - 1 && finalNumberPin ||
                    (mPassword && currentTag == mPinLength - 1 && finalNumberPin)) {
                if (editTextList.get(currentTag).length() > 0) {
                    editTextList.get(currentTag).setText("");
                }
                finalNumberPin = false;
            } else if (currentTag > 0) {
                mDelPressed = true;
                if (editTextList.get(currentTag).length() == 0) {
                    int lastTag = currentTag - 1;
                    //Takes it back one tile
                    editTextList.get(lastTag).requestFocus();
                    //Clears the tile it just got to
                    editTextList.get(lastTag).setText("");
                } else {
                    //If it has some content clear it first
                    editTextList.get(currentTag).setText("");
                }
            } else {
                //For the first cell
                if (editTextList.get(currentTag).getText().length() > 0)
                    editTextList.get(currentTag).setText("");
            }
            return true;
        }
        return false;
    }


    /**
     * Getters and Setters
     */
    private int getIndexOfCurrentFocus() {
        return editTextList.indexOf(currentFocus);
    }


    public int getSplitWidth() {
        return mSplitWidth;
    }

    public void setSplitWidth(int splitWidth) {
        this.mSplitWidth = splitWidth;
        int margin = splitWidth / 2;
        params.setMargins(margin, margin, margin, margin);

        for (TextField editText : editTextList) {
            editText.setLayoutConfig(params);
        }
    }

    public int getPinHeight() {
        return mPinHeight;
    }

    public void setPinHeight(int pinHeight) {
        this.mPinHeight = pinHeight;
        params.height = pinHeight;
        for (TextField editText : editTextList) {
            editText.setLayoutConfig(params);
        }
    }

    public int getPinWidth() {
        return mPinWidth;
    }

    public void setPinWidth(int pinWidth) {
        this.mPinWidth = pinWidth;
        params.width = pinWidth;
        for (TextField editText : editTextList) {
            editText.setLayoutConfig(params);
        }
    }

    public int getPinLength() {
        return mPinLength;
    }

    public void setPinLength(int pinLength) {
        this.mPinLength = pinLength;
        createEditTexts();
    }

    public boolean isPassword() {
        return mPassword;
    }

    public void setPassword(boolean password) {
        this.mPassword = password;
        setTransformation();
    }

    public String getHint() {
        return mHint;
    }

    public void setHint(String mHint) {
        this.mHint = mHint;
        for (TextField editText : editTextList) {
            editText.setHint(mHint);
        }
    }

    public int getPinBackground() {
        return mPinBackground;
    }

    public void setPinBackgroundRes(int res) {
        this.mPinBackground = res;
        for (TextField editText : editTextList) {
            editText.setBackground(new ShapeElement(getContext(), res));
        }
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        mClickListener = l;
    }

    public InputType getInputType() {
        return inputType;
    }

    public void setInputType(InputType inputType) {
        this.inputType = inputType;
        int it = getKeyboardInputType();
        for (TextField editText : editTextList) {
            editText.setTextInputType(it);
        }
    }

    public void setPinViewEventListener(PinViewEventListener listener) {
        this.mListener = listener;
    }
}
