# PinView

Pin code input control component, support any length and input any data.

## Design sketch
![](screenshots/PinView.gif)

## 1、Use steps
Solution 1:
1. Copy the PinView/library folder to the project directory
2. Modify settings.gradle under the project and add the dependency on this module as follows:
 ```
 include ':entry', ':library'
 ```
3. Introduce the dependency of the module under the project. Taking the entry module as an example, you need to modify the build.gradle file under the entry module to add the dependency:
 ```
 compile project(path: ':library')
 ```
 or
 ```
 implementation project(':library')
 ```

Solution 2: local use of har package integration
1. Compile this project, copy the har package generated in the build directory of the PinView/library folder to the project lib folder:
directory：\PinView\library\build\outputs\har\debug\library-debug.har
2. Add the following code in the entry's gradle
 ```
 implementation fileTree(dir:'libs', include: ['*.jar','*.har'])
 ```

Solution 3: 
1. Add this in your root `build.gradle` file (**not** your module `build.gradle` file):
```xml
allprojects {
	repositories {
		...
		mavenCentral()
	}
}
```
2. The lib is available on Maven Central, you can find it with [Gradle, please]
```xml
dependencies {
    implementation ‘com.gitee.baijuncheng-open-source:pin-view:1.0.0'
}
```

## 2、The best way to use it is to refer to demo
```java
    mPinView.setCanInput(true);// 设置是否可输入 默认true
    mPinView.setHint("");      // 设置EditText的Hint
    mPinView.setInputType(PinView.InputType.NUMBER); // 设置输入的格式
    mPinView.setPassword(true); // 设置是否以明文显示
    mPinView.setPinHeight(60);  // 设置每一个EditText的高度
    mPinView.setPinWidth(60);   // 设置每一个EditText的宽度
    mPinView.setPinLength(4);   // 设置可输入的 PIN 码长度，默认4
    mPinView.clearValue();      // 清除输入的数据
    mPinView.setValue("1234");  // 设置输入值
    mPinView.setPinViewEventListener(this); // 设置输入完毕的监听事件
```


## 3、Use in XML
```java
<com.nanchen.pinview.PinView
    ohos:id="$+id:pinView"
    ohos:width="match_content"
    ohos:height="match_content"
    ohos:layout_alignment="vertical_center"
    ohos:left_margin="20vp"
    app:cursorVisible="false"
    app:hint=""
    app:inputType="number"
    app:password="true"
    app:pinHeight="30vp"
    app:pinLength="4"
    app:pinWidth="30vp"
    app:splitWidth="20vp"/>
```

## License

    Copyright 2018 nanchen(刘世麟)
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.